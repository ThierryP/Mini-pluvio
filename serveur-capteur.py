#!/usr/bin/env python3
"""
Serveur écoutant les impulsions de l'objet capteur.
"""

import socketserver

from actions import stocker
from actions import créer_graphe

# Paramètres
HOST = "localhost"
PORT = 10000

class gestionnaire_impulsion(socketserver.BaseRequestHandler):
    """
    Classe de gestionnaire du serveur.

    Est instanciée à chaque implusion envoyée de par le capteur de pluie.
    Le traitement est réalisé en surchargeant la méthode .handle().
    """

    def handle(self):
        # self.request est la socket TCP connectée au client (le capteur de pluie).
        if self.request:
            print("Impulsion")
            stocker()
            créer_graphe()
        
if __name__ == "__main__":
    # Création de l'instance serveur attaché à IP/TCP
    socketserver.TCPServer.allow_reuse_address = True
    with socketserver.TCPServer((HOST, PORT), gestionnaire_impulsion) as serveur_pluviomètre:
        # Lance le server. Il est arrêté par Ctrl-C
        print("Lancement du serveur")
        serveur_pluviomètre.serve_forever()
